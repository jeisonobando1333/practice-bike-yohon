import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccessGuard } from './auth/guards/user-route-access.guard';
import { CreateAccountComponent } from './auth/create-account/create-account.component';


const routes: Routes = [
  {
    path: 'dashboard',
    data: {
      authorities: [Authority.ADMIN, Authority.USER] // se especifican los roles que pueden ingresar
    },
    canActivate: [UserRouteAccessGuard], // que el guardian verifique el rol
    loadChildren: () => import('./dashboard/dashboard.module')
      .then(m => m.DashboardModule)
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path:'access-denied',
    component:AccessDeniedComponent
  },
  {
    path:'login/create-account',
    component: CreateAccountComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
