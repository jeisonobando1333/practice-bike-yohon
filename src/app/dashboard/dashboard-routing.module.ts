import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';


const routes: Routes = [
  {
    path: '',
    component: MainDashboardComponent,
    children: [
      {
        path:'',
        redirectTo: 'dashboard',
        pathMatch:'full'
      },
      {
        path:'admin',
        loadChildren: () => import ('../modules/admin/admin.module')
        .then(m => m.AdminModule)
      },
      {
        path:'bikes',
        loadChildren: () => import ('../modules/bikes/bikes.module')
        .then(m => m.BikesModule)
      },
      {
        path:'clients',
        loadChildren: () => import ('../modules/clients/clients.module')
        .then(m => m.ClientsModule)
      },
      {
        path:'sales',
        loadChildren: () => import ('../modules/sales/sales.module')
        .then(m => m.SalesModule)
      },
      {
        path:'profile',
        loadChildren: () => import ('../modules/profile/profile.module')
        .then(m => m.ProfileModule)  
       }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
