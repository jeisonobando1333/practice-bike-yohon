import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/auth/login/login.service';
import { AccountService } from 'src/app/auth/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.styl']
})
export class NavbarComponent implements OnInit {
  /*VARS*/
  nameUser: string;
  emailUser:string;

  constructor(
    private loginService: LoginService,
    private account: AccountService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.nameUser = this.account.getUserName();
    this.emailUser = this.account.getEmail();
  }

  public logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }
}
