import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { AsideComponent } from './aside/aside.component';
import { AuthSharedModule } from '../auth/auth-shared/auth-shared.module';


@NgModule({
  declarations: [
    NavbarComponent, 
    MainDashboardComponent, 
    AsideComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule, 
    AuthSharedModule
  ]
})
export class DashboardModule { }
