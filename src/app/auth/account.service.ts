import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account } from './auth-shared/models/account.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { catchError, tap, shareReplay } from 'rxjs/operators';
import { ICredentials } from './credentials';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  /* Encargado de proveedor la informacion del usuario 
  ** Encargado de registrar nuevos usuarios
  */
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }
  // metodo para registrar usuario
  create(credentials: ICredentials): Observable<ICredentials> {
    return this.http.post<ICredentials>(`${environment.END_POINT}/api/account`, credentials);

  }
  // force => forzar la peticion que se va hacer mas adelante
  // tap => verifica que no tenga errores en la peticion
  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache || force || !this.isAthenticated()) {
      this.accountCache = this.fetch().pipe(catchError(() => {
        return of(null);
      }), tap((account: Account | null) => {
        this.authenticate(account);
        if (account) {
          this.router.navigate(['/dashboard']);
        }
      }), shareReplay()); //sincronizar todos los observables que se esten ejecuntando
    }
    return this.accountCache;
  }
  /*Para autenticar */
  authenticate(account: Account | null): void {
    this.userIdentity = account;
  }
  /*preguntar si esta autenticado */
  isAthenticated(): boolean {
    return this.userIdentity !== null;
  }
  /*Traer los datos de autenticacion */
  getUserName(): string {
    return this.userIdentity.firstName + ' ' + this.userIdentity.lastName;
  }
  getEmail():string {
    return this.userIdentity.email;
  }
  /*Obsevable => Pendiente si esta autenticado o No */
  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }
  /**
   * 
   * @param authorities pendiente verificacion de roles 
   */
  hasAnyAuthority(authorities: string[] | string): boolean {
    //this.userIdentity.authorities=>roles
    if (!this.userIdentity || !this.userIdentity.authorities) {
      return false;
    }
    // validar de que el parametro es un array  
    if (!Array.isArray(authorities)) {
      authorities = [authorities]; // convertir a un arreglo
    }
    // some => comprueba si alguno de los roles que se ingreso ,existe en la lista de roles permitidos
    // si encuentra el rol que se le paso como parametro deveuleve true, else false
    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
  }

  private fetch(): Observable<Account> {
    /// me retorna los datos del usuario autenticado
    return this.http.get<Account>(`${environment.END_POINT}/api/account`);
  }
}
