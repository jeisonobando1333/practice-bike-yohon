import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  
  constructor(){}

  /*ENCARGADO DE INTERCEPTAR CADA PETICION HTTP */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (
      !request 
      || !request.url 
      || (request.url.startsWith('http'))
    && !(environment.END_POINT && request.url.startsWith(environment.END_POINT))) {
      return next.handle(request); //si no se esta haciendo una peticion ,continua con la peticion

    }
    // Guardar token
    const token = localStorage.getItem('token') || sessionStorage.getItem('token');

    if (token) {

      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token
        }
      });


    }
    // continuar con el proceso normal
    return next.handle(request);
  }
}
