import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountService } from '../account.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserRouteAccessGuard implements CanActivate {

  constructor(
    private router: Router,
    private accountService: AccountService
  ) { }

  canActivate(
    //captura la ruta activa(ruta actual)//state => captita estado de la ruta
    route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const authorities = route.data['authorities'];
    return this.checkLogin(authorities, state.url);
  }
  checkLogin(authorities: string[], url: string): Observable<boolean> {
    return this.accountService.identity()
      // account => cuenta del usuario
      .pipe(map(account => {
        // compracion === , compara los tipos de datos y valores
        if (!authorities || authorities.length === 0) {
          return true;
        }
        if (account) {
          const hasAnyAuthority = this.accountService.hasAnyAuthority(authorities); //les paso los roles
          if (hasAnyAuthority) {
            return true; // solo si tiene rol especifico, redirecciona al dashboard
          }
          this.router.navigate(['access-denied']);
          return false;
        }
        this.router.navigate(['login']);
        return false;
      }));

  }

}
