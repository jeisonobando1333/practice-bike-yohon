import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { ICredentials, Credentials } from '../credentials';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {
  /*VARS*/
  statusSend: boolean = true;

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    rememberMe: false
  })

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  login() :void {
    const credentials: ICredentials = new Credentials();

    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;

    this.statusSend = true; // set status send , para que me cambie el valor

    this.loginService.login(credentials)
    .subscribe((res:any) => {
      /* Redirect to dashboard */
      console.warn(res);
      this.router.navigate(['dashboard']);
    },( error:any) => {
      if (credentials.password && credentials.username || (credentials.password || credentials.username)) {
        this.statusSend = false;
      }
      if (error.status === 401) {
        console.error("ERROR 401", error);
      }      
    }); 
  }
}
