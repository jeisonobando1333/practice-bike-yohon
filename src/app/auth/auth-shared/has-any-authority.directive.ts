import { Directive, OnDestroy, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountService } from '../account.service';

@Directive({
  selector: '[appHasAnyAuthority]'
})
export class HasAnyAuthorityDirective implements OnDestroy{

  private authorities: string[] =[];
  private authenticationSuscription?: Subscription;
  
  constructor(
    private accountService: AccountService,
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) { }

  @Input()
  set appHasAnyAuthority(authorities:string[] | string) {
    // se pasa un string a array 
    this.authorities = typeof authorities === 'string' ? [authorities] : authorities;// typeOf valua el tipo de dato que tiene

    this.updateView();
    this.authenticationSuscription = this.accountService.getAuthenticationState().subscribe( () => this.updateView());
  }

  ngOnDestroy(): void {
    // si esta suscripto , que se desuscripa
    if (this.authenticationSuscription) {
      this.authenticationSuscription.unsubscribe();
    }
  }

  private updateView(): void {
    const hasAnyAuthority = this.accountService.hasAnyAuthority(this.authorities);
    this.viewContainerRef.clear();
    if (hasAnyAuthority) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    }
  }
}
