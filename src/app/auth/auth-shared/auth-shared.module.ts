import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HasAnyAuthorityDirective } from './has-any-authority.directive';

@NgModule({
  declarations: [HasAnyAuthorityDirective],
  imports: [
    CommonModule
  ], 
  // permite el acceso a todos los componentes qeu quieran usar la directiva
  exports: [
    HasAnyAuthorityDirective
  ]
})
export class AuthSharedModule { }
