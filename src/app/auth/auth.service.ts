import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICredentials } from './credentials';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

   public login(credentials : ICredentials): Observable<any> {
     // {observe: 'response'} = verificar la respuesta de los headers http
     return this.http.post<any>(`${environment.END_POINT}/api/authenticate`, credentials, {observe: 'response'} )
    .pipe(map(res => {
      const bearerToken = res.headers.get('Authorization');

      if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
        const jwt = bearerToken.slice(7, bearerToken.length);
        this.storageAuthenticationToken(jwt, credentials.rememberMe);
        return jwt;
      }
    }));
   }

   /*Metodo login out = eliminar tokens*/
   public logout(): Observable<any> {
     return new Observable(observe => {
      localStorage.removeItem('token');
      sessionStorage.removeItem('token');
      observe.complete();
     });
   }

   //storage -- Authentication-- Token
   private storageAuthenticationToken(jwt: string, rememberMe: boolean): void {

    if (rememberMe) {
      localStorage.setItem('token', jwt);
      
    } else {
      sessionStorage.setItem('token', jwt);
    }

   }

}
