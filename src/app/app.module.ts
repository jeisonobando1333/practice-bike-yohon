import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './auth/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import {PasswordModule} from 'primeng/password';
import { AuthInterceptor } from './auth/guards/auth.interceptor';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { CreateAccountComponent } from './auth/create-account/create-account.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccessDeniedComponent,
    CreateAccountComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    PasswordModule /*import prime ng */

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, //tener en cuenta las peticiones
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
