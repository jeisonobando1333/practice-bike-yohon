import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainProfileComponent } from './main-profile/main-profile.component';
import { ProfileContentComponent } from './profile-content/profile-content.component';
import { ProfileUpdateComponent } from './profile-update/profile-update.component';


const routes: Routes = [
  {
    path:'',
    component: MainProfileComponent,
    children: [
      {
        path:'',
        component: ProfileContentComponent
      },
      {
        path:'update-profile',
        component: ProfileUpdateComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
