import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/auth/account.service';

@Component({
  selector: 'app-profile-content',
  templateUrl: './profile-content.component.html',
  styleUrls: ['./profile-content.component.styl']
})
export class ProfileContentComponent implements OnInit {

  /*VARS*/
  nameUser : string;
  emailUser: string;
  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    this.nameUser = this.accountService.getUserName();
    this.emailUser = this.accountService.getEmail();
    console.warn(this.nameUser)
  }

}
