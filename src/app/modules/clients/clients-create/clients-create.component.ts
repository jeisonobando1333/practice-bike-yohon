import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Client } from '../interfaces/clients';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients-create',
  templateUrl: './clients-create.component.html',
  styleUrls: ['./clients-create.component.styl']
})
export class ClientsCreateComponent implements OnInit {

  clientFormGroup: FormGroup;
  client: Client[];

  statusSearchDocument: boolean = true;

  formSearchClient = this.formBuilder.group({
    document:['']
  });

  constructor(private formBuilder: FormBuilder, private clientsService: ClientsService) { 
    this.clientFormGroup = this.formBuilder.group({
      name:[''],
      document:[''],
      email:[''],
      phoneNumber:[''],
      documentType:[''],
    });
  }

  ngOnInit() {
  }

  saveClient(){
    console.log('Datos', this.clientFormGroup.value);
    this.clientsService.saveClient(this.clientFormGroup.value)
    .subscribe(res => {
      console.warn('SAVE OK', res);
    }, error => {
      console.warn('Error', error);
    });
  }

  searchClient(): void {
    console.warn('Key Up');
    this.clientsService.query({
      'document.contains': this.formSearchClient.value.document
    })
    .subscribe(res => {
      this.client = res;
    })
  }
/*
  searchClient() {
    console.warn('Data', this.formSearchClient.value);
    this.clientsService.getClientByDocument(this.formSearchClient.value.document)
    .subscribe(res => {
      console.warn('Response bike by document', res);
      this.client = res;
      this.statusSearchDocument = true;
    }, error => {
      console.warn('No encontrado', error);
      this.client = null;
      this.statusSearchDocument = false;
    });
  }
*/
}
