import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';
import { MainClientsComponent } from './main-clients/main-clients.component';
import { Authority } from 'src/app/auth/auth-shared/constants/authority.constants';
import { UserRouteAccessGuard } from 'src/app/auth/guards/user-route-access.guard';


const routes: Routes = [
 {
   path:'',
   component: MainClientsComponent,
   data: {
    authorities: [Authority.ADMIN] // se especifican los roles que pueden ingresar
  },
  canActivate: [UserRouteAccessGuard], // que el guardian verifique el rol
   children: [
    {
      path: '',
      component: ClientsListComponent
    },
    {
      path: 'clients-create',
      component: ClientsCreateComponent
    },
    {
      path: 'clients-update',
      component: ClientsUpdateComponent
    }
   ]
 }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
