import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from './interfaces/clients';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/request.utils';


@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<Client[]>{
    let params = createRequestParams(req);
    return this.http.get<Client[]>(`${environment.END_POINT}/api/clients`,{params: params})
    .pipe(map(res =>{
      return res;
    }));
  }

  public saveClient(client: Client): Observable<Client> {
    return this.http.post<Client>(`${environment.END_POINT}/api/clients`, client)
    .pipe(map(res => {
      return res;
    }));
  }

  public getClientById(id: string): Observable<Client> {
    return this.http.get<Client>(`${environment.END_POINT}/api/clients/${id}`)
    .pipe(map(res => {
      return res;
    })); 
  }

  public updateClient(client: Client): Observable<Client> {
    return this.http.put<Client>(`${environment.END_POINT}/api/clients`, client)
    .pipe(map(res => {
      return res;
    }));
  }
/*
  public getClientByDocument(document: string): Observable<Client>{
    let params = new HttpParams();
    params = params.append('document', document);
    console.warn('Params', params);
    return this.http.get<Client>(`${environment.END_POINT_PRUEBA}/api/clients/find-by-document`, {params: params})
      .pipe(map(res => {
        return res;
      }));
  }
*/

  public deleteItem(id: string): Observable<Client> {
    return this.http.delete<Client>(`${environment.END_POINT}/api/clients/${id}`)
    .pipe(map(res => {
      return res;
    }))
  }

}
