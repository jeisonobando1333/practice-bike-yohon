import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductSalesComponent } from './product-sales/product-sales.component';
import { AdminRoutingModule } from './admin-routing.module';



@NgModule({
  declarations: [ProductSalesComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
