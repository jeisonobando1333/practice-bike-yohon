import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductSalesComponent } from './product-sales/product-sales.component';



const routes: Routes = [
  {
    path: 'product-sales',
    component: ProductSalesComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
