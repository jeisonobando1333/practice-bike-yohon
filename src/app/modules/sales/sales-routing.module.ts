import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaleCreateComponent } from './sale-create/sale-create.component';
import { SaleListComponent } from './sale-list/sale-list.component';
import { MainSalesComponent } from './main-sales/main-sales.component';


const routes: Routes = [
  
  {
    path: '',
    component:MainSalesComponent,
    children:[
      {
        path: '',
        component: SaleCreateComponent 
      },
      {
        path: 'sale-list',
        component: SaleListComponent
      }
    ]
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
