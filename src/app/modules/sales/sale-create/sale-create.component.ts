import { Component, OnInit } from '@angular/core';
import { Bike } from '../../bikes/interfaces/bike';
import { SalesService } from '../sales.service';
import { BikesService } from '../../bikes/bikes.service';
import { ISale } from '../interfaces/sale';
import { ClientsService } from '../../clients/clients.service';
import { Client } from '../../clients/interfaces/clients';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sale-create',
  templateUrl: './sale-create.component.html',
  styleUrls: ['./sale-create.component.styl']
})
export class SaleCreateComponent implements OnInit {

  bikesList: Bike[];
  salesList: ISale[] = [];
  clientList: Client[] = [];
  clientSelectedTemp: Client;
  total: number = 0;
  
  searchForm = this.fb.group({
    document: ''
  });

  constructor(private salesService: SalesService,
    private bikesService: BikesService,
    private clientsService: ClientsService,
    private fb: FormBuilder
    ) { }

  ngOnInit(): void {
    this.bikesService.getDataQueryBikes()
      .subscribe(res => {
        this.bikesList = res;
      });
  }

  

  addClient(client: Client): void {
    this.clientSelectedTemp = client;
    console.warn('Selected client', this.clientSelectedTemp);
  }

  buyItem(bike: Bike): void {
    this.total += bike.price;
    console.warn('Selected', bike);
    this.salesList.push({
      clientId: this.clientSelectedTemp.id,
      clientName:this.clientSelectedTemp.name,
      bikeId: bike.id,
      bikeSerial: bike.serial,
      bike: bike,
    });
  }

  searchClient(): void {
    console.warn('Key Up');
    this.clientsService.query({
      'document.contains': this.searchForm.value.document
    })
    .subscribe(res => {
      this.clientList = res;
    })
  }

}







