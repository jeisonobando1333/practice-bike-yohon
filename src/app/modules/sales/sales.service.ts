import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bike } from '../bikes/interfaces/bike';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ISale } from './interfaces/sale';
import { Client } from '../clients/interfaces/clients';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(private http: HttpClient) { }

  public query(): Observable<ISale[]>{
    return this.http.get<ISale[]>(`${environment.END_POINT}/api/sales`)
    .pipe(map(res =>{
      return res;
    }));
  }

  
  public buyItem(id: string): Observable<Bike[]> {
    return this.http.get<Bike[]>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

  public addClient(id: string): Observable<Client[]> {
    return this.http.get<Client[]>(`${environment.END_POINT}/api/clients/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

}
