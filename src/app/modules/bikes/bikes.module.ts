import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BikesListComponent} from './bikes-list/bikes-list.component';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';
import { BokesViewComponent } from './bokes-view/bokes-view.component';
import { BikesRoutingModule } from './bikes-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MainBikesComponent } from './main-bikes/main-bikes.component';
import {InputSwitchModule} from 'primeng/inputswitch';


@NgModule({
  declarations: [
    BikesListComponent, 
    BikesCreateComponent, 
    BikesUpdateComponent, 
    BokesViewComponent, 
    BokesViewComponent, 
    MainBikesComponent
  ],
  imports: [
    CommonModule,
    BikesRoutingModule,
    ReactiveFormsModule,
    InputSwitchModule
  ]
})
export class BikesModule { }
