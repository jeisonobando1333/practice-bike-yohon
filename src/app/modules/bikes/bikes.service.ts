import { Injectable } from '@angular/core';
import { Bike } from './interfaces/bike';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/request.utils';

@Injectable({
  providedIn: 'root'
})

export class BikesService {

  constructor(private http: HttpClient) { }

  public getDataQueryBikes(req?: any): Observable<Bike[]>{
    let params = createRequestParams(req);
      return this.http.get<Bike[]>(`${environment.END_POINT}/api/bikes`,{params: params})
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * guardar 
   * @param bike 
   */
  public saveBike(bike: Bike): Observable<Bike> {
    return this.http.post<Bike>(`${environment.END_POINT}/api/bikes`, bike)
    .pipe(map(res => {
      return res;
    }));
  }


  /**
   * This method is for getting one bike for id
   * @param id 
   */
  public getBikeById(id: string): Observable<Bike> {
    return this.http.get<Bike>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    })); 
  }


  /**
   * this...UPDATE 
   * @param bike 
   */
  public updateBike(bike: Bike): Observable<Bike> {
    return this.http.put<Bike>(`${environment.END_POINT}/api/bikes`, bike)
    .pipe(map(res => {
      return res;
    }));
  }

  public getBikeBySerial(serial: string): Observable<Bike>{
    let params = new HttpParams();
    params = params.append('serial', serial);
    console.warn('Params', params);
    return this.http.get<Bike>(`${environment.END_POINT}/api/bikes/find-bike-by-serial`, {params: params})
      .pipe(map(res => {
        return res;
      }));
  }


  public deleteItem(id: string): Observable<Bike> {
    return this.http.delete<Bike>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }
 
}//close key class Service
